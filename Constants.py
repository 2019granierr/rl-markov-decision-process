WHITE_COLOR_RGB = (255,255,255)
BLACK_COLOR_RGB = (0,0,0)
RED_COLOR_RGB = (255, 0, 0)
GREEN_COLOR_RGB = (0, 255, 0)
BLUE_COLOR_RGB = (0, 0, 255)
LOW_ICE_COLOR_RGB = (193, 239, 255)
VERY_ICE_COLOR_RGB = (21, 195, 255)
YELLOW_COLOR_RGB = (234, 255, 21)
WIDTH = 500
NUM_ROWS = 10
NUM_EPISODES=10000
EPSILON = 3 # Integer 1-10. 2 = 20% random, 3 = 30% random ...
CELL_SIZE = WIDTH/NUM_ROWS
ACTIONS = ['left', 'right', 'up', 'down']
TERMINAL_CELLS = [(4, 5), (7, 4)]
RANDOM_FACTOR = 0.1

CELL_VALUES = [
((2, 1), -40),
((2, 2), -40),
((2, 3), -40),
((2, 4), -40),
((2, 6), -10),
((2, 7), -10),
((2, 8), -10),
((2, 9), -5),
((4, 5), 30),
((7, 4), 50)]

CELL_WALLS = [(2,5)]

# ( (coordY, coordX), "upshift"/"downshift"/"leftshift"/"rightshift")
#upshift according to Pygame's Oy downwards pointing axis means going DOWN
CELL_SHIFTS = {
    (3,0) : "upshift",
    (3,4) : "leftshift", 
    (4,2) : "leftshift",
    (4,3) : "leftshift",
    (6,6) :"upshift",
    (3,4) : "leftshift"
}

CELL_ICE = {
    (6,3) : "very_icy",
    (4,8) : "low_icy",
    (1,1) : "low_icy"
}
