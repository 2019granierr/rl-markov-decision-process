from Constants import NUM_ROWS, CELL_VALUES, TERMINAL_CELLS, CELL_WALLS, CELL_SHIFTS, CELL_ICE, RANDOM_FACTOR, ACTIONS
import numpy as np
import random


class Board(object):
    cellValues = {} # Maps cell (x, y) to reward r

    def __init__(self, numRows = NUM_ROWS):
        self.numRows = numRows
        self.initCellRewards()

    def initCellRewards(self):
        for xPos in range(NUM_ROWS):
            for yPos in range(NUM_ROWS):
                if (xPos, yPos) not in CELL_WALLS:
                    self.cellValues[(xPos, yPos)] = 0

    def createPenaltyCells(self):
        for cell, val in CELL_VALUES:
            self.cellValues[cell[0], cell[1]] = val

    def isTerminalCell(self, coord):
        return coord in TERMINAL_CELLS

    # IMPORTANT: this function only takes into account the basic movement, not the shifts/ice afterwards.
    # Therefore, shifts and ice can cause game crash. It is up to the game designer to be careful.
    # We could also catch the exception and consider that the agent "lost" the game if he ends up in an illegal position.
    def isValidCell(self, coord, action):
        (xCoord, yCoord) = coord
        if action == 'left':
            xCoord-=1
        elif action == 'right':
            xCoord+=1
        elif action == 'up':
            yCoord+=1
        elif action == 'down':
            yCoord-=1

        isValid = 0 <= xCoord < NUM_ROWS  and 0 <= yCoord < NUM_ROWS and (xCoord, yCoord) not in CELL_WALLS
        return isValid

    def getCellAfterAction(self, coord, action):

        def applyAction(action, coord):
            (xCoord, yCoord) = coord
            if action == 'left':
                xCoord-=1
            elif action == 'right':
                xCoord+=1
            elif action == 'up':
                yCoord+=1
            elif action == 'down':
                yCoord-=1
            return (xCoord, yCoord)
    
        isRandom = None # Boolean to be assigned value
        e = random.random()
        validActions = list(filter(lambda action: self.isValidCell(coord, action), ACTIONS))
        if e > RANDOM_FACTOR or validActions == [action]:
            coord = applyAction(action, coord)
            isRandom = False

        else:
            other_actions = validActions.copy()
            other_actions.remove(action)
            random_action = random.choice(other_actions)
            coord = applyAction(random_action, coord)
            isRandom = True
        
        while coord in self.getShiftCellsMap().keys() or coord in self.getIceCellsMap().keys():

            if coord in self.getShiftCellsMap().keys():
                coord = self.getCellAfterShift(coord)

            if coord in self.getIceCellsMap().keys():
                coord = self.getCellAfterIce(coord)
        
        # This loop makes possible long chains of agent manipulation by the environment
    
        return (coord, isRandom)

    

    def getCellValue(self, coord):
        return self.cellValues[coord]

    # At this point these squares mustn't be put on the border (otherwise game crashes are likely)
    def getCellAfterShift(self, coord):
        xCoord, yCoord = coord
        shift = CELL_SHIFTS[coord]
        if shift == "leftshift":
            return (xCoord-2, yCoord)
        elif shift == "rightshift":
            return (xCoord+2, yCoord)
        elif shift == "downshift":
            return (xCoord, yCoord-2)
        else: # upshift
            return (xCoord, yCoord+2)

    # At this point these squares mustn't be put on the border (otherwise game crashes are likely)
    def getCellAfterIce(self, coord):
        xCoord, yCoord = coord
        ice = CELL_ICE[coord]
        if ice == "very_icy":
            xShift, yShift = 0, 0
            while xShift == 0 and yShift == 0:
                xShift = np.random.randint(-2,2)
                yShift = np.random.randint(-2,2)
            return(xCoord + xShift, yCoord + yShift)
        else : #low_icy
            xShift, yShift = 0, 0
            while xShift == 0 and yShift == 0:
                xShift = np.random.randint(-1,1)
                yShift = np.random.randint(-1,1)
            return(xCoord + xShift, yCoord + yShift)


    def getCells(self):
        return self.cellValues.keys()

    def getCellMap(self):
        return self.cellValues

    def getRewardCellsMap(self):
        return {cell: val for cell, val in self.cellValues.items() if val > 0}

    def getPenaltyCellsMap(self):
        return {cell: val for cell, val in self.cellValues.items() if val < 0}

    def getCellWalls(self):
        return CELL_WALLS
    
    def getShiftCellsMap(self):
        return CELL_SHIFTS
    
    def getIceCellsMap(self):
        return CELL_ICE

