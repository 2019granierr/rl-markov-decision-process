from Window import Window
from Board import Board
from Player import Player
from QLearner import QLearner
import pygame
import time

pygame.init()
w = Window()
p = Player()
b = Board()
b.createPenaltyCells()
print("Running programm.")

while True:
    q = QLearner(b)
    qTable = q.learn()
    dirToGo = {}
    for k, v in qTable.items():
        dirToGo[k] = max(v, key=v.get)

    print("Simulation starting")
    # Resetting player
    p.updateCurrPos((0,0))
    p.score = 0

    while(not b.isTerminalCell(p.currPos)):
        # Entire grid is redrawn at each move as a temporary solution to erase the player's previous positions
        w.drawSurface(b, p)
        
        # taking a move
        time.sleep(1)
        (nextCell, isRandom) = b.getCellAfterAction(p.currPos, dirToGo[p.currPos])
        p.updateCurrPos(nextCell)
        p.updateScore(b.getCellValue(p.currPos))
        print("Score: " + str(p.score))
        currNode = p.getCurrCoords()

        # If the agent has been victim of random influence of the environment, he will show up with an @ of confusement.
        w.drawPlayer(p, isRandom)


        pygame.display.update()

    # should reinitialize there
